<?php

// conditional enqueue styles
function example_enqueue_styles() {
	
	$deps = false;

	if (is_child_theme()) {
		
		$deps = array('parent-styles');
		
		// load parent styles if active child theme
		wp_enqueue_style('parent-styles', trailingslashit(get_template_directory_uri()) .'style.css', false);
		
	}
	
	// load active theme stylesheet
	wp_enqueue_style('theme-styles', get_stylesheet_uri(), $deps);
	
}
add_action('wp_enqueue_scripts', 'example_enqueue_styles');
?>